(function() {
	var questions = [
		[
			'What famous organisation is the largest manufacturer of tyres?',
			'Lego'
		],
		[
			'What animal placed in milk will keep the milk fresh?',
			'frogs'
		],
		[
			'In Vietnam, what is the Vietnam War usually called?',
			'American War'
		],
		[
			'Which famous swimmer was allergic to chlorine?',
			'Ian Thorpe'
		],
		[
			'Who was a famous painter and a suspect for the disappearance of the Mona Lisa?',
			'Picasso'
		],
		[
			'What trait in women are Orangutans seemingly attracted to?',
			'red hair'
		],
		[
			'Which country has the longest (half mile) swimming pool?',
			'Chile'
		],
		[
			'Which country name means "Land of the Aryans"?',
			'Iran'
		],
		[
			'Where did fortune cookies originate from?',
			'USA'
		],
	];

	var index = 0; // current question being printed out

	$(document).ready(function() {
		// can I get the length of var questions?
		$(".intro-btn").focus();
		var qList = getQuestions();
		for(var i = 0; i < qList.length; i++) {
			var q = questions[i];
			var $qBlock = $("<li></li>").addClass("list-group-item");
			$qBlock.append(q[0]);
			$qBlock.append(" <strong>"+q[1]+"</strong>");

			var $q2Block = $("<li></li>").addClass("list-group-item");
			$q2Block.append(q[0]);
			var $br = $("<br>").addClass("visibile-xs visible-sm");
			$q2Block.append($br);
			$q2Block.append('&nbsp; <input type="text" class="q2-answer">');

			$(".allquestions-ul").append($qBlock);
			$(".q2-ul").append($q2Block);
		}
	});

	function getQuestions() {
		var qList = [];
		questions.forEach(function (item) {
			qList.push(item[0]);
		});
		return qList;
	}

	function getAnswers() {
		var qList = [];
		questions.forEach(function (item) {
			qList.push(item[1]);
		});
		return qList;
	}

	function loadQuestion() {
		var q = getQuestions();
		if (index < q.length) {
			$(".question-block").html(
				q[index]+"</p>"
			);
		} 
		else {
			$(".questions").slideUp();
			$(".allquestions").css("display", "block");
			setTimeout(function() {
				$(".allquestions-btn").focus();
			}, 100);
			$("body").scrollTop(0);
		}
	}

	$(".start-btn").click(function() {
		location.reload();
	})

	$(".intro-btn").click(function() {
		$(".intro").slideUp();
		$(".questions").show('fast', function() {
			$(this).css("display", "block");
				$(".questions-answer").focus();
		});
	});

	$(".questions-answer").bind("keypress", function(e) {
		if (e.keyCode === 13) {
			$(".questions-answer").val('').focus(); // flush contents of input box, as answers aren't being checked the first time
			index += 1;
			loadQuestion();
		}
	});

	$(".questions-btn").click(function() {
		$(".questions-answer").val('').focus(); // flush contents of input box, as answers aren't being checked the first time
		index += 1;
		loadQuestion();
	});

	
	$(".allquestions-btn").click(function() {
		$(".allquestions").slideUp(function() {
			$("body").scrollTop(0);
		});
		$(".questions2").css("display", "block");
		$(".q2-btn").hide();
		$(".q2-answer").first().focus();

		$(".q2-answer").each(function() {
			$(this).bind("keypress", function(e) {
				if (e.keyCode === 13 || e.type === "click") {
					if(!($(this).parent().is(":last-child"))) {
						$(this).parent().next().find(".q2-answer").focus();
					}
					else {
						$(".q2-btn-check").focus();
					}
				}
			});
		});
	});

	$(".q2-btn-check").on("click keypress", function(e) {
		$(this).off('click keypress');
		if (e.type === "click" || e.keyCode === 13) {
			var answers = getAnswers();
			$(".q2-answer").each(function(index) {
				var answerScore = $(this).val().toLowerCase().score(answers[index].toLowerCase(), 0.6);
				if (answerScore > 0.5) {
					$(this).parent().append('&emsp; <span class="correct glyphicon glyphicon-ok-circle"></span>');
				}
				else {
					$(this).parent().append('&emsp; <span class="incorrect glyphicon glyphicon-remove-circle"></span>' + '&emsp; <span>'+answers[index]+'</span>');
				}
			$(".q2-btn-check").hide();	
			$(".question2-title").html("Check the answers you got wrong with the ones shown");
			$(".q2-btn").css("display", "initial").focus();
			});
		}
	});

	$(".q2-btn").on("click keypress", function(e) {
		if (e.type === "click" || e.keyCode === 13) {
			$(".questions2").slideUp();
			$(".conclusion").css("display", "block");
		}

	});

	loadQuestion();
}());